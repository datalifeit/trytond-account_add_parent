datalife_account_add_parent
===========================

The account_add_parent module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_add_parent/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_add_parent)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
